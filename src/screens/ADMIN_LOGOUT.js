import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as postsActions from 'redux/modules/logout';


class Posts extends Component {

  componentDidMount() {
    this.next();
  }

  next() {
 
    let { listPosts } = this.props;

    listPosts();
  }

  render() {
    return (
      <div className="posts">
      
      </div>
    );
  }
}

export default connect(
  state => ({ posts: state.posts }),
  dispatch => bindActionCreators(postsActions, dispatch)
)(Posts);
