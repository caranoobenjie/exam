import axios from 'axios';

import config from 'config';

const SET = 'lig/posts/SET';

const initialState = [ ];

export default function reducer(state = initialState, action = {}) {
  switch(action.type) {
    case SET:
      return action.posts;
    default:
      return state;
  }
}

export function listPosts(page = 1) {
  return (dispatch) => {
    axios({
      baseURL: config.api,
      url: '/logout',
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then((res) => {
      localStorage.clear(); 
    });
  };
}

