import { combineReducers } from 'redux';

import user from './user';
import post from './post';
import posts from './posts';
import comments from './comments';
import logout from './logout';

export default combineReducers({
  user,
  post,
  posts,
  comments,
  logout
});
