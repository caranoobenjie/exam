<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Response;
use App\Posts;
use App\User;
use App\Comments;
use App\Fileupload;
use Helper;
use DB;

class PostsController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth:api')->except(['index','show_comment']);
    }

    public $successStatus   = 201;
    public $failStatus      = 401;
    
    public function index()
    {
        $post = Posts::paginate(2);
        return response()->json([
            'data' =>  $post
        ]);
    }

    public function show($slug)
    {

        $blog = Posts::where('slug', $slug)->first();

        if (Auth::user()->id !== $blog->user_id)
        {
            return response()->json([
                'errors' => 'You can only edit your own post.'
            ], 403);
        }else{
            return response()->json([
                'data' => Posts::where('slug', $slug)->where('user_id',$blog->user_id)->first()
            ]);
        }
    }

    public function store(Request $request)
    {
 
        $fileName = NULL;

        if($request->get('image'))
        {
           $image = $request->get('image');
           $fileName = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];

          \Image::make($request->get('image'))->save(public_path('blog_images/').$fileName);
        }


        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'title'   => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                "message"   => "The given data was invalid.",
                "errors"    =>  $validator->errors()
            ], 201);
            return;
        }

        $request->request->add(['image' => $request->root().'/blog_images/'.$fileName]);
        
        $posts = new Posts($request->all());
        $posts->user_id = Auth::user()->id;
        $posts->save();

        return response()->json(['id' => $posts->id], $this->successStatus);
    }

    public function update(Request $request, $update)
    {

        $fileName = NULL;
        if($request->get('image'))
        {
           $image = $request->get('image');
           $fileName = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];

          \Image::make($request->get('image'))->save(public_path('blog_images/').$fileName);
        }

        $posts = Posts::where('slug', $update)->first();
        $update_comp = Posts::updateOrCreate([
            'id'   => $posts->id,
            ],[
                'id'   => $posts->id,
                'title' => $request->input('title'),
                'content'=> $request->input('content'),
                'image' => $request->root().'/blog_images/'.$fileName
            ]);
        return response()->json([
            'id' => $update_comp
        ], $this->successStatus);
    }

    public function delete_post($id)
    {
        Posts::find($id)->delete();
        Comments::where('creator_id',$id)->delete();
        return response()->json([
            'success'   => true,
            'status'    => "record deleted successfully",
        ], $this->failStatus);
    }

    public function show_comment($get_data){
    
        $post_data = Posts::where('slug', $get_data)->first();
        return response()->json([
            'data' => Comments::where('posts_id', $post_data->id)->orderBy('id', 'DESC')->get(),
        ]);
    }

    public function store_comment(Request $request , $store_comment){
      
        $validator = Validator::make($request->all(), [
            'body'      => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                "message"   => "The given data was invalid.",
                "errors"    =>  $validator->errors()
            ], 201);
            return;
        }

        $post = Posts::where('slug', $store_comment)->first();
        $comment = new Comments();
        $comment->posts_id = $post->id;
        $comment->creator_id = (!empty(Auth::user()->id))  ? Auth::user()->id : NULL;
        $comment->body = $request->body;
        $post->comments()->save($comment);

        return response()->json($comment, 201);
    }

    public function delete_comment($post_slug,$commnent_id)
    {
        $delete_comment = Comments::find($commnent_id)->delete();
      
        return response()->json([
            'success'   => true,
            'status'    => "record deleted successfully",
        ], $this->failStatus);
    }
}
