<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Validator;  
use Carbon\Carbon;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:api')->except(['login','register']);
    }
    public $successStatus   = 201;
    public $failStatus      = 401;

    public function login(Request $request){ 

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if( Auth::attempt($credentials) ){ 
            $user           = $request->user();
            $tokenResult    = $user->createToken('Personal Access Token');
            $token          = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(2);
    
            $token->save();
    
            return response()->json([
                'token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'user_id' => $user->id
            ]);
        } 
        else{ 

            $validator = Validator::make($request->all(), [ 
                'email'   => 'required',
                'password'=> 'required',
            ]);

            if ($validator->fails())
            {
                return response()->json([
                    "message" => "The given data was invalid.",
                    "errors" =>  $validator->errors()
                ], $this->failStatus);
                return;
            }else{
                return response()->json([
                    "message" => "The given data was invalid.",
                    "errors" => array('auth' => array('Authentication failed.'))
                ], $this->failStatus);
                return;
            }
        } 
    }

    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name'          => 'required|string|max:255',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6',
            'password_confirmation' => 'required|string|min:6|same:password', 
        ]);

        if ($validator->fails())
        {
            return response()->json([
                "message" => "The given data was invalid.",
                "errors" =>  $validator->errors()
            ], $this->failStatus);
        }
    
        $request['password']    = bcrypt($request['password']);
        $user                   = User::create($request->toArray());
        return response()->json(['id' => $user->id], $this->successStatus);
    }

    public function logout(){
        if (Auth::check()) {
            $user = Auth::user()->token();
            $user->revoke();

            return response()->json([
                "success" => $user,
                'message' => 'Successfully logged out'
            ],201);
        }
    }
}
