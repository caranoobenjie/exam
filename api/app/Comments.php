<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Posts;
class Comments extends Model
{
    protected $fillable = [];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function commentable()
    {
        return $this->morphTo();
    }
   
}
