<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS");
header("Access-Control-Allow-Headers: Accept, Authorization, Content-Type, X-Requested-With");
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([
    'prefix' => ''
], function () {
    Route::post('login', 'API\UserController@login');
    Route::post('register', 'API\UserController@register');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('details', 'API\UserController@details');
        Route::get('logout', 'API\UserController@logout');
    });
});





Route::prefix('posts')->group(function () {
    /*post area */
    Route::get('/', 'PostsController@index')->name('post.main');
    Route::get('/{slug}', 'PostsController@show')->name('post.get');
    Route::post('/', 'PostsController@store')->name('store.post');
    Route::post('/{update}', 'PostsController@update')->name('post.update');
    Route::delete('/{id}', 'PostsController@delete_post')->name('post.delete');

    /*comment area */
    Route::get('/{get_data}/comments', 'PostsController@show_comment')->name('comment.get');
    Route::post('/{post_data}/comments', 'PostsController@store_comment')->name('comment.post');
    Route::delete('/{post_slug}/comments/{commnent_id}' , 'PostsController@delete_comment')->name('comment.delete');
});